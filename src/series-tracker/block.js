/**
 * BLOCK: ixtutor/series-tracker
 */

import './style.scss';
import './editor.scss';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { Component } = wp.element;

registerBlockType( 'ixtutor/series-tracker', {
	title: __( 'ixtutor/series-tracker' ),
	icon: 'palmtree',
	category: 'common',
	keywords: [
		__( 'ixtutor-series-tracker' ),
		__( 'series' ),
		__( 'tracker' )
	],

	attributes: {
	},

	edit: class extends Component {
		render() {
			return (
				<div class="series-tracker-box">
				Series Tracker
				</div>
			)
		}
	},

	save: function(props) {
		return null;
	},
} );
