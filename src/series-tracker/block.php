<?php

function render_block_ixtutor_series_tracker($attributes, $content) {
    $post_series_id = get_field("series_id");
    $post_part_id = get_field("part_id");
    return <<<HTML
<script>
var seriesList = [
    {
        "seriesId":"learn-tensorflow-by-example", 
        "seriesTitle": "Learn Tensorflow by examples",
        "parts": [
            {
                "partId": 1,
                "title": "Tensorflow fundamentals",
                "link": "/learning-tensorflow-by-example-part-1-basics/"
            },
            {
                "partId": 2,
                "title": "Linear Regression with Tensorflow",
                "link": "/linear-regression-with-tensorflow-(with-interactive-code-examples)/"
            }
        ]
    }
]

function getPartElement(partId, title, link) {
  var li = document.createElement("li");

  label = "<span class='ix-series-part-label'>Part " + partId + ": </span>";

  if (link === null) {
    title = "<span class='ix-series-part-coming-soon-title'>" + title + " </span>";
  } else if (partId == "{$post_part_id}") {
    title = "<b><span class='ix-series-part-coming-soon-title'>" + title + ' <span style="color:green">&#9745;</span></span></b>';
  } else {
    title = "<a class='ix-series-link-element' href='" + link + "'>" + title + "</a>";
  }

  li.innerHTML= label + title;
  li.classList.add('ix-part-list-element')
  return li;
}

function findSeries(seriesId) {
    for (var series of seriesList) {
        if (series.seriesId === seriesId) return series;
    }
}

function init() {
    series = findSeries('{$post_series_id}')

    lastPart = 0;
    for (var part of series.parts) {
        document.getElementById('ix-series-tracker-part-list')
            .appendChild(getPartElement(part.partId, part.title, part.link));
        lastPart = part.partId;
    }

    document.getElementById('ix-series-tracker-part-list')
            .appendChild(getPartElement(lastPart + 1, "&lt;Coming soon&gt;", null));
    document.getElementById('ix-series-tracker-heading').innerHTML = "Tutorial series - <u>" + series.seriesTitle + "</u>";
}

window.onload = init
</script>
<div class="ix-series-tracker">
<h5 id="ix-series-tracker-heading" class="ix-series-tracker-heading">Article Series:</h5>
<ol id="ix-series-tracker-part-list" class="ix-series-tracker-part-list">
</ol>
</div>
HTML;
}

register_block_type( 'ixtutor/series-tracker', array(
    'render_callback' => 'render_block_ixtutor_series_tracker',
) );