<?php
function getFormattedPreCode($code) { return ($code == '' ? $code : $code.'
'); };
function getFormattedPostCode($code) { return ($code == '' ? $code : '
'.$code); };

function render_block_ixtutor_code_run($attributes, $content) {
    $prefix = 'pref'.bin2hex(openssl_random_pseudo_bytes(10));

    $code = $attributes["code"];

    $preCode = isset($attributes["preCode"]) ? $attributes["preCode"] : "";
    $postCode = isset($attributes["postCode"]) ? $attributes["postCode"] : "";    
    $fullCode = getFormattedPreCode($preCode).$code.getFormattedPostCode($postCode);

    $output = $attributes["output"];

    $runButtonDisplay = "inline";
    $fullCodeButtonDisplayOnCompactView = "inline";
    
    if ($output == "") 
        $runButtonDisplay = "none";

    if ($preCode == "" && $postCode == "") {
        $fullCodeButtonDisplayOnCompactView = "none";
    }
    return <<<HTML
<script>
    {$prefix}isDirty = false;
    {$prefix}isCorrect = false;
    
    function {$prefix}showOutput() {
        document.getElementById("{$prefix}-svg-running").style.display="none";
        document.getElementById("{$prefix}-i-running").style.display="none";
        document.getElementById("{$prefix}-run-output").style.display="inline";
    }
    
    function {$prefix}enableRunMode() {
        document.getElementById("{$prefix}-btn-run").style.display="none";
        document.getElementById("{$prefix}-svg-running").style.display="inline";
        document.getElementById("{$prefix}-i-running").style.display="inline";
    
        setTimeout("{$prefix}showOutput()", 1500);
    }
    
    function {$prefix}compactMode() {
        jQuery("#{$prefix}-full-code").fadeOut(300, function() {
            jQuery("#{$prefix}-compact-code").fadeIn(300);
        });
    }
    
    function {$prefix}fullMode() {
        jQuery("#{$prefix}-compact-code").fadeOut(300, function() {
            jQuery("#{$prefix}-full-code").fadeIn(300);
        });
    }
    
</script>
<div class="code-outer-box">
    <!-- Compact mode -->
    <div id="{$prefix}-compact-code" id= class="code-inner-box">
        <pre class="line-numbers code-pre">
<codex class="language-python code-box">{$code}</codex>
</pre>
        <span class="code-view-changer" onclick="{$prefix}fullMode()" style="display:{$fullCodeButtonDisplayOnCompactView};"><u><small>Full code</small></u></span>
    </div>

    <!-- Full mode -->
    <div id="{$prefix}-full-code" style="display:none" class="code-inner-box">
        <pre class="line-numbers code-pre">
<codex class="language-python code-box">{$fullCode}</codex>
</pre>
        <span class="code-view-changer" onclick="{$prefix}compactMode()"><u><small>Compact code</small></u></span>
    </div>

    <!-- Common elements -->
    <button style="display:{$runButtonDisplay}" id="{$prefix}-btn-run" class="blue-button" onclick="{$prefix}enableRunMode()">Run >></button>
    <svg style="display:none" id="{$prefix}-svg-running" width="40px" height="40px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-ripple" style="animation-play-state: running; animation-delay: 0s; background: none;"><circle cx="50" cy="50" r="20.3565" fill="none" ng-attr-stroke="{{config.c1}}" ng-attr-stroke-width="{{config.width}}" stroke="#0483e5" stroke-width="2" style="animation-play-state: running; animation-delay: 0s;"><animate attributeName="r" calcMode="spline" values="0;40" keyTimes="0;1" dur="1" keySplines="0 0.2 0.8 1" begin="-0.5s" repeatCount="indefinite" style="animation-play-state: running; animation-delay: 0s;"></animate><animate attributeName="opacity" calcMode="spline" values="1;0" keyTimes="0;1" dur="1" keySplines="0.2 0 0.8 1" begin="-0.5s" repeatCount="indefinite" style="animation-play-state: running; animation-delay: 0s;"></animate></circle><circle cx="50" cy="50" r="37.6717" fill="none" ng-attr-stroke="{{config.c2}}" ng-attr-stroke-width="{{config.width}}" stroke="#cfcfcf" stroke-width="2" style="animation-play-state: running; animation-delay: 0s;"><animate attributeName="r" calcMode="spline" values="0;40" keyTimes="0;1" dur="1" keySplines="0 0.2 0.8 1" begin="0s" repeatCount="indefinite" style="animation-play-state: running; animation-delay: 0s;"></animate><animate attributeName="opacity" calcMode="spline" values="1;0" keyTimes="0;1" dur="1" keySplines="0.2 0 0.8 1" begin="0s" repeatCount="indefinite" style="animation-play-state: running; animation-delay: 0s;"></animate></circle></svg>
    <i style="display:none" id="{$prefix}-i-running">running..</i>
    <pre style="display:none" id="{$prefix}-run-output">
{$output}
</pre>
</div>
HTML;

}


register_block_type( 'ixtutor/code-run', array(
    'render_callback' => 'render_block_ixtutor_code_run',
) );