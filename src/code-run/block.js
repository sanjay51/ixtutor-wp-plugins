/**
 * BLOCK: ixtutor/code-run
 */

import './style.scss';
import './editor.scss';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { PlainText } = wp.editor;
const { Component } = wp.element;

registerBlockType( 'ixtutor/code-run', {
	title: __( 'ixtutor/code-run' ),
	icon: 'palmtree',
	category: 'common',
	keywords: [
		__( 'ixtutor-code-run' ),
		__( 'question' ),
		__( 'answer' )
	],

	attributes: {
		preCode: {
			type: 'string',
			default: ''
		},
		code: {
			type: 'string',
			default: ''
		},
		postCode: {
			type: 'string',
			default: ''
		},
		output: {
			type: 'string',
			default: '',
		}
	},

	edit: class extends Component {
		constructor(props) {
			super(...arguments);
			this.props = props;

			this.onPreCodeChange = this.onPreCodeChange.bind(this);
			this.onCodeChange = this.onCodeChange.bind(this);
			this.onPostCodeChange = this.onPostCodeChange.bind(this);
			this.onOutputChange = this.onOutputChange.bind(this);
		}

		onPreCodeChange(preCode) { this.props.setAttributes({ preCode }) };
		onCodeChange(code) { this.props.setAttributes({ code }) }
		onPostCodeChange(postCode) { this.props.setAttributes({ postCode }) };
		onOutputChange(output) { this.props.setAttributes({output}) };

		render() {
			const {preCode, code, postCode, output} = this.props.attributes;
			const {className} = this.props;

			return (
				<div className = {className}>
					Pre Code: <PlainText rows="2" value={preCode} onChange = {this.onPreCodeChange} />
					Code: <PlainText rows="5" value={code} onChange = {this.onCodeChange} />
					Post Code: <PlainText rows="2" value={postCode} onChange = {this.onPostCodeChange} />
					Output: <PlainText value={output} onChange = {this.onOutputChange} />
					<br/>
				</div>
			)
		}
	},

	save: function(props) {
		return null;
	}
} );
