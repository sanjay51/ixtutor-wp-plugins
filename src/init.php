<?php

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function ixtutor_gutenberg_blocks_assets() { // phpcs:ignore
	// Styles.
	wp_enqueue_style(
		'ixtutor_gutenberg_blocks-style-css', // Handle.
		plugins_url( 'dist/blocks.style.build.css', dirname( __FILE__ ) ), // Block style CSS.
		array( 'wp-editor' ) // Dependency to include the CSS after it.
	);

	wp_enqueue_style(
		'ixtutor_gutenberg_blocks-prism-css', // Handle.
		plugins_url( 'src/code-run/prism/prism.css', dirname( __FILE__ ) ), // Block style CSS.
		array( 'wp-editor' ) // Dependency to include the CSS after it.
	);
	http://localhost:8888/wp-includes/js/jquery/jquery.js?ver=1.12.4

	wp_enqueue_script(
		'ixtutor_gutenberg_blocks-prism-js', // Handle.
		plugins_url( 'src/code-run/prism/prism.js', dirname( __FILE__ ) ), // Block.build.js: We register the block here. Built with Webpack.
		array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor' ), // Dependencies, defined above.
		true // Enqueue the script in the footer.
	);

	wp_enqueue_script("jquery");
}

add_action( 'enqueue_block_assets', 'ixtutor_gutenberg_blocks_assets' );

function ixtutor_gutenberg_blocks_editor_assets() { // phpcs:ignore
	// Scripts.
	wp_enqueue_script(
		'ixtutor_gutenberg_blocks-block-js', // Handle.
		plugins_url( '/dist/blocks.build.js', dirname( __FILE__ ) ), // Block.build.js: We register the block here. Built with Webpack.
		array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor' ), // Dependencies, defined above.
		true // Enqueue the script in the footer.
	);

	// Styles.
	wp_enqueue_style(
		'ixtutor_gutenberg_blocks-editor-css', // Handle.
		plugins_url( 'dist/blocks.editor.build.css', dirname( __FILE__ ) ), // Block editor CSS.
		array( 'wp-edit-blocks' ) // Dependency to include the CSS after it.
	);
}

// Hook: Editor assets.
add_action( 'enqueue_block_editor_assets', 'ixtutor_gutenberg_blocks_editor_assets' );

// Block template initialization
require_once plugin_dir_path( __FILE__ ) . 'question-answer/block.php';
require_once plugin_dir_path( __FILE__ ) . 'code-run/block.php';
require_once plugin_dir_path( __FILE__ ) . 'series-tracker/block.php';