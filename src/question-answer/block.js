/**
 * BLOCK: ixtutor/question-answer
 */

import './style.scss';
import './editor.scss';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { PlainText } = wp.editor;
const { Component } = wp.element;

registerBlockType( 'ixtutor/question-answer', {
	title: __( 'ixtutor/question-answer' ),
	icon: 'palmtree',
	category: 'common',
	keywords: [
		__( 'ixtutor-question-answer' ),
		__( 'question' ),
		__( 'answer' )
	],

	attributes: {
		question: {
			type: 'string'
		},
		answer: {
			type: 'string',
			default: 'answer',
		},
		answerExplanation: {
			type: 'string',
			default: 'answerExplanation',
		},
		option1: {
			type: 'string',
			default: 'option1',
		},
		option2: {
			type: 'string',
			default: 'option2',
		},
		option3: {
			type: 'string',
			default: 'option3',
		},
		option4: {
			type: 'string',
			default: 'option4',
		},
	},

	edit: class extends Component {
		constructor(props) {
			super(...arguments);
			this.props = props;

			this.onQuestionChange = this.onQuestionChange.bind(this);
			this.onOption1Change = this.onOption1Change.bind(this);
			this.onOption2Change = this.onOption2Change.bind(this);
			this.onOption3Change = this.onOption3Change.bind(this);
			this.onOption4Change = this.onOption4Change.bind(this);
			this.onAnswerChange = this.onAnswerChange.bind(this);
			this.onAnswerExplanationChange = this.onAnswerExplanationChange.bind(this);
		}

		onQuestionChange(question) { this.props.setAttributes({ question }) }
		onOption1Change(option1) { this.props.setAttributes({option1}) };
		onOption2Change(option2) { this.props.setAttributes({option2}) };
		onOption3Change(option3) { this.props.setAttributes({option3}) };
		onOption4Change(option4) { this.props.setAttributes({option4}) };
		onAnswerChange(answer) { this.props.setAttributes({answer: answer.target.value}) };
		onAnswerExplanationChange(answerExplanation) { this.props.setAttributes({answerExplanation}) };

		render() {
			const {question, answer, option1, option2, option3, option4, answerExplanation} = this.props.attributes;
			const {className} = this.props;

			return (
				<div className = {className}>
					Question: <PlainText value={question} onChange = {this.onQuestionChange} />
					Option 1: <PlainText value={option1} onChange = {this.onOption1Change} />
					Option 2: <PlainText value={option2} onChange = {this.onOption2Change} />
					Option 3: <PlainText value={option3} onChange = {this.onOption3Change} />
					Option 4: <PlainText value={option4} onChange = {this.onOption4Change} />
					Answer: <br/>
					<select onChange = {this.onAnswerChange}>
						<option value="1" selected={answer == "1"}>Option 1</option>
  						<option value="2" selected={answer == "2"}>Option 2</option>
  						<option value="3" selected={answer == "3"}>Option 3</option>
  						<option value="4" selected={answer == "4"}>Option 4</option>
					</select><br/>
					Answer explaination: <PlainText value={answerExplanation} onChange = {this.onAnswerExplanationChange} />
					<br/>
				</div>
			)
		}
	},

	save: function(props) {
		return null;
	},
} );
