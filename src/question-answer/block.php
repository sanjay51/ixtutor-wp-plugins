<?php

function render_block_ixtutor_question_answer($attributes, $content) {
    $prefix = 'pref'.bin2hex(openssl_random_pseudo_bytes(10));

    $answer = $attributes["answer"];
    $question = $attributes["question"];
    $option1 = $attributes["option1"];
    $option2 = $attributes["option2"];
    $option3 = $attributes["option3"];
    $option4 = $attributes["option4"];
    $answerExplanation = $attributes["answerExplanation"];
    
    return sprintf('
<script>
'.$prefix.'isDirty = false;
'.$prefix.'isCorrect = false;

function '.$prefix.'ix_evaluateOption(actual) {
    answer = "'.$answer.'";
    document.getElementById("'.$prefix.'answerExplanationDiv").style.display = "inline"
    if (answer == actual) {
        isCorrect = true
        document.getElementById("'.$prefix.'answerCorrectLabel").style.display = "inline"
        document.getElementById("'.$prefix.'answerIncorrectLabel").style.display = "none"
    } else {
        isCorrect = false
        document.getElementById("'.$prefix.'answerIncorrectLabel").style.display = "inline"
        document.getElementById("'.$prefix.'answerCorrectLabel").style.display = "none"
    }
}

</script>

<div class="ixtutor-question-answer-block">
<div class="ix-question">
<b>Q.</b> <b>'.$question.'</b>
</div>

<div class="ix-options">
<div>
<input class="ix-answer-option" type="radio" onClick="'.$prefix.'ix_evaluateOption(1)" id="'.$prefix.'option1" name="'.$prefix.'option" value="'.$prefix.'option1" />
<label class="ix-answer-option" style="display: inline;" for="'.$prefix.'option1">'.$option1.'</label>
</div>

<div>
<input class="ix-answer-option" type="radio" onClick="'.$prefix.'ix_evaluateOption(2)" id="'.$prefix.'option2" name="'.$prefix.'option" value="'.$prefix.'option2">
<label class="ix-answer-option" style="display: inline;" for="'.$prefix.'option2">'.$option2.'</label>
</div>

<div>
<input class="ix-answer-option" type="radio" onClick="'.$prefix.'ix_evaluateOption(3)" id="'.$prefix.'option3" name="'.$prefix.'option" value="'.$prefix.'option3">
<label class="ix-answer-option" style="display: inline;" for="'.$prefix.'option3">'.$option3.'</label>
</div>

<div>
<input class="ix-answer-option" type="radio" onClick="'.$prefix.'ix_evaluateOption(4)" id="'.$prefix.'option4" name="'.$prefix.'option" value="'.$prefix.'option4">
<label class="ix-answer-option" style="display: inline;" for="'.$prefix.'option4">'.$option4.'</label>
</div>

<div id="'.$prefix.'answerExplanationDiv" style="display:none">
<b id="'.$prefix.'answerCorrectLabel" style="background-color: lightgreen">Correct!</b>
<b id="'.$prefix.'answerIncorrectLabel" style="background-color: orange">Incorrect!</b>
'.$answerExplanation.'
</div>
</div>

</div>
    '
    );
}

register_block_type( 'ixtutor/question-answer', array(
    'render_callback' => 'render_block_ixtutor_question_answer',
) );